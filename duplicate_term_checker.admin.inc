<?php

/**
 * @file
 * Provides functions for Administration settings.
 */

/**
 * Duplicate term checker duplicacy settings form.
 */
function duplicate_term_checker_form($form, &$form_state) {

  // Extract all vocabularies.
  $vocabularies = taxonomy_get_vocabularies();
  $options = array();
  foreach ($vocabularies as $vocabulary) {
    $options[$vocabulary->vid] = $vocabulary->name;
  }

  $form['duplicate_term_checker_all_vocabs'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Apply for specific vocabulary:'),
    '#default_value' => variable_get('duplicate_term_checker_all_vocabs', array()),
    '#options' => $options,
    '#description' => t('If no option is selected, it will be checked for all vocabularies.'),
  );

  $form['duplicate_term_checker_applied_for'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Check for duplicate:'),
    '#default_value' => variable_get('duplicate_term_checker_applied_for', array()),
    '#options' => array(
      'terms_within_vocab' => t('Terms within a vocabulary'),
      'terms_within_term' => t('Terms within a term'),
    ),
  );
  return system_settings_form($form);
}
