
INTRODUCTION
------------

The Duplicate Term Checker module provides a feature to check duplicate term 
within a vocabulary and duplicate term within a term.

REQUIREMENTS
------------

This module requires the following module:

 * Taxonomy (https://drupal.org/project/taxonomy)

INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module.
Extract the 'duplicate_term_checker' folder and put in to modules directory

CONFIGURATION
-------------

  * Configure settings at Admin » Config » Content Authoring » 
Duplicate Term Checker

  - OR After installation, click on "Configure" link.
